package shortenurl


def longUrl = "http://grails.org"

def shortUrl1 = TinyUrl.shorten(longUrl)
println shortUrl1

def shortUrl2 = IsGd.shorten(longUrl)
println shortUrl2

println ""

def shortenUrlService = new ShortenUrlService()

println shortenUrlService.isgd(longUrl)
println shortenUrlService.tinyurl(longUrl)
