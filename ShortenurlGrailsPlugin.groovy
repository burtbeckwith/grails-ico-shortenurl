class ShortenurlGrailsPlugin {
	def version = "0.1"
	def grailsVersion = "2.0 > *"
	def title = "IC Shortenurl Plugin"
	def author = "Alexey Lovchikov"
	def authorEmail = "alexey.lovchikov@gmail.com"
	def description = 'Brief summary/description of the plugin.'
	def documentation = "http://grails.org/plugin/shortenurl"
	def pluginExcludes = [
		"src/groovy/shortenurl/Test.groovy"
	]

	def license = "APACHE"
	def issueManagement = [system: "JIRA", url: "https://bitbucket.org/aldeveloper/grails-ico-shortenurl/issues"]
	def scm = [url: "https://bitbucket.org/aldeveloper/grails-ico-shortenurl"]
}
