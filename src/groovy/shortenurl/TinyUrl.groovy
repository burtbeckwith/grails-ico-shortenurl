package shortenurl

class TinyUrl {
	
	static String shorten(String longUrl){
		"http://tinyurl.com/api-create.php?url=${longUrl}".toURL().text
	}
}
