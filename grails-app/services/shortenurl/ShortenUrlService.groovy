package shortenurl

class ShortenUrlService {

	static transactional = false

	String tinyurl(String longUrl) {
		TinyUrl.shorten(longUrl)
	}

	String isgd(String longUrl) {

		String shortUrl = IsGd.shorten(longUrl)

		if(shortUrl.contains("error")){
			log.error(shortUrl)
		}
      
		return shortUrl
	}
}
