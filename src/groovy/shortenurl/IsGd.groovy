package shortenurl

class IsGd {

	static String shorten(String longUrl){

		String addr = "http://is.gd/api.php?longurl=${longUrl}"
		def urlConnection = addr.toURL().openConnection()
		if(urlConnection.responseCode == 200){
			return urlConnection.content.text
		}
		return "An error occurred: ${addr}\n${urlConnection.responseCode} : ${urlConnection.responseMessage}"
	}
}
