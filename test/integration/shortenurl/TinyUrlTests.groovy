package shortenurl

class TinyUrlTests extends GroovyTestCase {

	static transactional = false

	void test() {
		String shortUrl = TinyUrl.shorten("http://grails.org")
		assertEquals "http://tinyurl.com/3xfpkv", shortUrl
	}
}
